<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>matriz1.php</title>
    </head>
    <body>
        <?php
        DEFINE(FILAS, 5);
        DEFINE(COLUMNAS, 5);

        $matriz = array();
        $contador = 0;
        for ($i = 0; $i < FILAS; $i++) {
            for ($j = 0; $j < COLUMNAS; $j++) {
                if ($contador < 10) {
                    $matriz[$i][$j] = "0" . $contador;
                } else {
                    $matriz[$i][$j] = $contador;
                }
                $contador++;
            }
        }
        for ($i = 0; $i < FILAS; $i++) {
            for ($j = 0; $j < COLUMNAS; $j++) {
                echo "  " . $matriz[$i][$j];
            }
            echo "<br>";
        }
        ?>
    </body>
</html>
