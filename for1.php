<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>while1.php</title>
    </head>
    <body>
        <?php
        echo "<table border=1>";
        $tabla = 10;
        $i = 1;
        while ($i <= 10) {
            echo "<tr>";
            echo "<td>", "$tabla*$i", "</td>";
            echo "<td>", $tabla * $i, "</td>";
            $i++;
        }
        echo "</tr>";
        echo "</table>";
        ?>
    </body>
</html>
