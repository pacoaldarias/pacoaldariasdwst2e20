<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>array1.php</title>
    </head>
    <body>
        <?php
        $a = array(5, 7, 9, 13);
        $suma = 0;
        $elementos = count($a);
        for ($i = 0; $i < $elementos; $i++) {
            $suma = $suma + $a[$i];
        }
        $media = $suma / $elementos;
        echo "La media es: " . $media;
        ?>
    </body>
</html>
