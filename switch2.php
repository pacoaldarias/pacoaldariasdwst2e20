<html>
    <head>
        <title>swith2.php</title>
    </head>
    <body>
        <?php
        $vehiculo = "camion";
        echo "La variable posicion es ", $vehiculo;
        echo "<br>";

        switch ($vehiculo) {
            case "camion":
                echo "La variable contiene el valor de camion";
                break;
            case "coche":
                echo "La variable contiene el valor de coche";
                break;
            default:
                echo "La variable contiene otro valor distinto";
        }
        ?>
    </body>
</html>
