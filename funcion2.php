<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        function referencia(&$c2) {
            $c2 .= "por referencia.";
        }

        $c1 = "Esto es un parámetro  ";
        referencia($c1);
        echo $c1;
        ?>

    </body>
</html>
